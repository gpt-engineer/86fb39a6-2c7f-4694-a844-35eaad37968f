let startTime;
let endTime;
let reactionTimes = [];
let bestTime = Number.MAX_VALUE;

const clickBox = document.getElementById("clickBox");
const reactionTimeDisplay = document.getElementById("reactionTime");

const startButton = document.getElementById("startButton");

startButton.addEventListener("click", () => {
  startButton.classList.add("hidden");
  clickBox.classList.remove("hidden");
  setTimeout(
    () => {
      clickBox.classList.remove("bg-blue-500");
      clickBox.classList.add("bg-red-500");
      startTime = new Date();
    },
    Math.random() * 3000 + 1000,
  );
});

const historyList = document.getElementById("historyList");

clickBox.addEventListener("click", () => {
  if (clickBox.classList.contains("bg-red-500") && !clickBox.classList.contains("hidden")) {
    // Stop the timer and calculate reaction time
    endTime = new Date();
    const reactionTime = endTime - startTime;
    reactionTimes.push(reactionTime);
    reactionTimeDisplay.textContent = `Your reaction time is ${reactionTime.toFixed(2)} ms`;

    // Update history
    const listItem = document.createElement("li");
    listItem.textContent = `${reactionTime.toFixed(2)} ms`;
    historyList.prepend(listItem);

    // Limit history to last 5 tries
    while (historyList.children.length > 5) {
      historyList.removeChild(historyList.lastChild);
    }

    // Reset the box color and hide it after a delay
    setTimeout(() => {
      clickBox.classList.remove("bg-red-500");
      clickBox.classList.add("bg-blue-500");
      clickBox.classList.add("hidden");
      startButton.classList.remove("hidden");
    }, 2000);
  }
});
